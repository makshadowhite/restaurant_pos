<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Royal Restaurant</title>
        
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="icon" href="{{asset('public/admin/img/favicon.ico')}}" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="{{asset('public/admin/css/bootstrap.min.css')}}" />
        <link rel="stylesheet" href="{{asset('public/admin/css/bootstrap-responsive.min.css')}}" />
        <link rel="stylesheet" href="{{asset('public/admin/css/colorpicker.css')}}" />
        <link rel="stylesheet" href="{{asset('public/admin/css/datepicker.css')}}" />
        <link rel="stylesheet" href="{{asset('public/admin/css/uniform.css')}}" />
        <link rel="stylesheet" href="{{asset('public/admin/css/matrix-style.css')}}" />
        <link rel="stylesheet" href="{{asset('public/admin/css/matrix-media.css')}}" />
        <link rel="stylesheet" href="{{asset('public/admin/css/bootstrap-wysihtml5.css')}}" />
        <link href="{{asset('public/admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="{{asset('public/admin/css/jquery-ui.css')}}">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{asset('public/admin/css/jquery-ui.css')}}">
        
        
        
    </head>
    <body>

        <!--Header-part-->
        <div id="header">
            <h1></h1>
        </div>
        <!--close-Header-part--> 