        <!--Footer-part-->
        <div class="row-fluid">
            <div id="footer" class="span12"> 
                <p>2017 &copy; Royal Restaurant.</p>
                <p>Developed by <a href="http://shadowhiteanimators.com">shadowhiteanimators.com</a></p>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="{{asset('public/admin/js/jquery.min.js')}}"></script> 
        <script src="{{asset('public/admin/js/jquery.ui.custom.js')}}"></script> 
        <script src="{{asset('public/admin/js/bootstrap.min.js')}}"></script> 
        <script src="{{asset('public/admin/js/jquery.uniform.js')}}"></script> 
        <script src="{{asset('public/admin/js/select2.min.js')}}"></script> 
        <script src="{{asset('public/admin/js/jquery.dataTables.min.js')}}"></script> 
        <script src="{{asset('public/admin/js/matrix.js')}}"></script> 
        <script src="{{asset('public/admin/js/matrix.tables.js')}}"></script>
        <script src="{{asset('public/admin/js/bootstrap-datepicker.js')}}"></script>
    </body>
</html>